
public class ObjectNotFoundException extends LinkedListException{
	public ObjectNotFoundException() {
		super();
	}
	
	public ObjectNotFoundException(String msg) {
		super(msg);
	}
}
