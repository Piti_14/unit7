
public class InvalidIndexException extends LinkedListException{
	public InvalidIndexException() {
		super();
	}
	
	public InvalidIndexException(String msg) {
		super(msg);
	}
}
