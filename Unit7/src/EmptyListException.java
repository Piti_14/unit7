
public class EmptyListException extends ObjectNotFoundException{
	public EmptyListException() {
		super();
	}
	
	public EmptyListException(String msg) {
		super(msg);
	}
}
