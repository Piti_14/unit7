

public class LinkedList {
	private Element first, last;
	private int size;
	
	public LinkedList() {
		first = new Element(null);
		first.setNext(last);
		last = new Element(null);
		size = 2;
	}
	
	public void insertFirst(Object o) {
		Element e = new Element(o);
		e.setNext(first);
		first = e;
		size++;
	}
	
	public void insertLast(Object o) {
		Element e = new Element(o);
		last.setNext(e);
		last = e;
		size++;
	}
	
	public void print() {
		Element e = first;
		for(int i = 0; i < size; i++) {
			System.out.println(e.getObject().toString());
			e = e.getNext();
		}
	}
	
	public boolean isEmpty() {
		if(size == 0) {
			return true;
		} else {
			return false;
		}
	}
	
	public void remove(Object o) throws ObjectNotFoundException, EmptyListException{
		if(isEmpty()) {
			System.out.println("The list is empty");
		} else {
			Element i = first;
			Element ant = first;
			if(i.getObject() == o) {
				first.setObject(first.getNext());
				i.setNext(null);
			} else {
				i = i.getNext();
				while(i.getObject() != null) {
					if(i.getObject() == o) {
						ant.setNext(i.getNext());
						i.setNext(null);
					} else {
						ant = i;
						i = i.getNext();
					}
				}
			}
		}		
	}
	
	
	public Object getFirstObject() throws EmptyListException{
		if(isEmpty()) {
			System.out.println("The list is empty.");
		}
	}
}
