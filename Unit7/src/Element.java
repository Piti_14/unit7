
public class Element {
	private Object object;
	private Element next;
	
	public Element(Object newObject) {
		object = newObject;
		next = null;
	}
	
	public Object getObject() {
		return object;
	}
	
	public Element getNext() {
		return next;
	}
	
	public void setNext(Element e) {
		next = e;
	}
	
	public void setObject(Object o) {
		object = o;
	}
	
	public void delete() {
		object = null;
		next = null;
	}
}
